package local.kamil.collibrachallenge.command;

import static local.kamil.collibrachallenge.command.CommandType.UNKNOWN;

public class UnknownCommand extends Command {

	UnknownCommand() {
		super(UNKNOWN);
	}
}

package local.kamil.collibrachallenge.command;

abstract class NodeCommand extends Command {

	private final String nodeName;

	public NodeCommand(CommandType type, String nodeName) {
		super(type);
		this.nodeName = nodeName;
	}

	public String getNodeName() {
		return nodeName;
	}
}

package local.kamil.collibrachallenge.command;

import static local.kamil.collibrachallenge.command.CommandType.BYE;

public class ByeCommand extends Command {

	public ByeCommand() {
		super(BYE);
	}
}

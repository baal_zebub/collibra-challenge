package local.kamil.collibrachallenge.command;

abstract class BiNodeCommand extends Command {

	private final String sourceNodeName;
	private final String targetNodeName;

	protected BiNodeCommand(CommandType type, String sourceNodeName, String targetNodeName) {
		super(type);

		this.sourceNodeName = sourceNodeName;
		this.targetNodeName = targetNodeName;
	}

	public String getSourceNodeName() {
		return sourceNodeName;
	}

	public String getTargetNodeName() {
		return targetNodeName;
	}
}

package local.kamil.collibrachallenge.command;

public enum CommandType {

	HELLO,
	ADD_NODE,
	ADD_EDGE,
	REMOVE_NODE,
	REMOVE_EDGE,
	BYE,
	SHORTEST_PATH,
	CLOSER_THAN,
	UNKNOWN
}

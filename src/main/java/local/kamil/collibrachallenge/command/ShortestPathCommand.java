package local.kamil.collibrachallenge.command;

public class ShortestPathCommand extends BiNodeCommand {

	public static final String REGEX = "SHORTEST PATH (?<sourceNode>[\\p{Alnum}\\-]+) (?<targetNode>[\\p{Alnum}\\-]+)";

	protected ShortestPathCommand(String sourceNodeName, String targetNodeName) {
		super(CommandType.SHORTEST_PATH, sourceNodeName, targetNodeName);
	}
}

package local.kamil.collibrachallenge.command;

import static local.kamil.collibrachallenge.command.CommandType.ADD_EDGE;

public class AddEdgeCommand extends BiNodeCommand {

	static final String REGEX = "ADD EDGE (?<sourceNode>[\\p{Alnum}\\-]+) (?<targetNode>[\\p{Alnum}\\-]+) (?<weight>[0-9]+)";

	private final int weight;

	AddEdgeCommand(String sourceNodeName, String targetNodeName, int weight) {
		super(ADD_EDGE, sourceNodeName, targetNodeName);
		this.weight = weight;
	}

	public int getWeight() {
		return weight;
	}
}

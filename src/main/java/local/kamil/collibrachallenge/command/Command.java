package local.kamil.collibrachallenge.command;

public abstract class Command {

	private final CommandType type;

	protected Command(CommandType type) {
		this.type = type;
	}

	public CommandType getType() {
		return type;
	}
}

package local.kamil.collibrachallenge.command;

public class CloserThanCommand extends NodeCommand {

	public static final String REGEX = "CLOSER THAN (?<weight>[0-9]+) (?<name>[\\p{Alnum}\\-]+)";
	private final int weight;

	protected CloserThanCommand(String nodeName, int weight) {
		super(CommandType.CLOSER_THAN, nodeName);
		this.weight = weight;
	}

	public int getWeight() {
		return weight;
	}
}

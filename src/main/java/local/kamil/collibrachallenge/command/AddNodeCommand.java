package local.kamil.collibrachallenge.command;

import static local.kamil.collibrachallenge.command.CommandType.ADD_NODE;

public class AddNodeCommand extends NodeCommand {

	static final String REGEX = "ADD NODE (?<name>[\\p{Alnum}\\-]+)";

	protected AddNodeCommand(String nodeName) {
		super(ADD_NODE, nodeName);
	}
}

package local.kamil.collibrachallenge.command;

public class RemoveNodeCommand extends NodeCommand {

	static final String REGEX = "REMOVE NODE (?<name>[\\p{Alnum}-]+)";

	protected RemoveNodeCommand(String nodeName) {
		super(CommandType.REMOVE_NODE, nodeName);
	}
}

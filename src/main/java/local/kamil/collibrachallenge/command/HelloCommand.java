package local.kamil.collibrachallenge.command;

import static local.kamil.collibrachallenge.command.CommandType.HELLO;

public class HelloCommand extends Command {

	static final String REGEX = "^HI, I AM (?<name>[\\p{Alnum}\\-]+)";

	private final String clientName;

	HelloCommand(String clientName) {
		super(HELLO);
		this.clientName = clientName;
	}

	public String getClientName() {
		return clientName;
	}
}

package local.kamil.collibrachallenge.command;

import java.util.Arrays;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public enum CommandFactory {

	HELLO {

		@Override
		protected boolean canCreateFrom(String message) {
			return message.matches(HelloCommand.REGEX);
		}

		@Override
		protected Command doCreateCommandFrom(String message) {
			return new HelloCommand(getClientName(message));
		}

		private String getClientName(String message) {
			Matcher matcher = prepareMatcher(message, HelloCommand.REGEX);
			return matcher.group("name");
		}
	},

	ADD_NODE {

		@Override
		protected boolean canCreateFrom(String message) {
			return message.matches(AddNodeCommand.REGEX);
		}

		@Override
		protected Command doCreateCommandFrom(String message) {
			return new AddNodeCommand(getNodeName(message));
		}

		private String getNodeName(String message) {
			Matcher matcher = prepareMatcher(message, AddNodeCommand.REGEX);
			return matcher.group("name");
		}
	},

	REMOVE_NODE {

		@Override
		protected boolean canCreateFrom(String message) {
			return message.matches(RemoveNodeCommand.REGEX);
		}

		@Override
		protected Command doCreateCommandFrom(String message) {
			return new RemoveNodeCommand(getNodeName(message));
		}

		private String getNodeName(String message) {
			Matcher matcher = prepareMatcher(message, RemoveNodeCommand.REGEX);
			return matcher.group("name");
		}
	},

	ADD_EDGE {

		@Override
		protected boolean canCreateFrom(String message) {
			return message.matches(AddEdgeCommand.REGEX);
		}

		@Override
		protected Command doCreateCommandFrom(String message) {
			Matcher matcher = prepareMatcher(message, AddEdgeCommand.REGEX);

			final String sourceEdgeName = matcher.group("sourceNode");
			final String targetEdgeName = matcher.group("targetNode");
			final int weight = Integer.parseInt(matcher.group("weight"));

			return new AddEdgeCommand(sourceEdgeName, targetEdgeName, weight);
		}
	},

	REMOVE_EDGE {

		@Override
		protected boolean canCreateFrom(String message) {
			return message.matches(RemoveEdgeCommand.REGEX);
		}

		@Override
		protected Command doCreateCommandFrom(String message) {
			Matcher matcher = prepareMatcher(message, RemoveEdgeCommand.REGEX);

			final String sourceEdgeName = matcher.group("sourceNode");
			final String targetEdgeName = matcher.group("targetNode");

			return new RemoveEdgeCommand(sourceEdgeName, targetEdgeName);
		}
	},

	SHORTEST_PATH {

		@Override
		protected boolean canCreateFrom(String message) {
			return message.matches(ShortestPathCommand.REGEX);
		}

		@Override
		protected Command doCreateCommandFrom(String message) {
			Matcher matcher = prepareMatcher(message, ShortestPathCommand.REGEX);

			final String sourceEdgeName = matcher.group("sourceNode");
			final String targetEdgeName = matcher.group("targetNode");

			return new ShortestPathCommand(sourceEdgeName, targetEdgeName);
		}
	},

	CLOSER_THAN {

		@Override
		protected boolean canCreateFrom(String message) {
			return message.matches(CloserThanCommand.REGEX);
		}

		@Override
		protected Command doCreateCommandFrom(String message) {
			Matcher matcher = prepareMatcher(message, CloserThanCommand.REGEX);

			final String sourceNodeName = matcher.group("name");
			int weight = Integer.parseInt(matcher.group("weight"));

			return new CloserThanCommand(sourceNodeName, weight);
		}
	},

	BYE {

		@Override
		protected boolean canCreateFrom(String message) {
			return "BYE MATE!".matches(message);
		}

		@Override
		protected Command doCreateCommandFrom(String message) {
			return new ByeCommand();
		}
	},

	UNKNOWN {

		@Override
		protected boolean canCreateFrom(String message) {
			return true;
		}

		@Override
		protected Command doCreateCommandFrom(String message) {
			return new UnknownCommand();
		}
	};

	public static Command create(String message) {
		return Arrays.stream(values())
			.filter(type -> type.canCreateFrom(message))
			.findFirst()
			.map(type -> type.createMessageFrom(message))
			.orElseThrow(() -> new IllegalArgumentException(String.format("unable to create a Command for message \"%s\"", message)));
	}

	protected abstract boolean canCreateFrom(String message);

	protected Command createMessageFrom(String message) {
		if (canCreateFrom(message)) {
			return doCreateCommandFrom(message);
		}

		throw new IllegalArgumentException(String.format("message \"%s\" does not match the type", message));
	}

	protected abstract Command doCreateCommandFrom(String message);

	private static Matcher prepareMatcher(String message, String regex) {
		Matcher matcher = Pattern.compile(regex).matcher(message);
		matcher.matches();

		return matcher;
	}
}

package local.kamil.collibrachallenge.command;

public class RemoveEdgeCommand extends BiNodeCommand {

	static final String REGEX = "REMOVE EDGE (?<sourceNode>[\\p{Alnum}\\-]+) (?<targetNode>[\\p{Alnum}\\-]+)";

	public RemoveEdgeCommand(String sourceNodeName, String targetNodeName) {
		super(CommandType.REMOVE_EDGE, sourceNodeName, targetNodeName);
	}
}

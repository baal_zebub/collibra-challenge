package local.kamil.collibrachallenge.command;

import java.io.PrintWriter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.BiConsumer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import local.kamil.collibrachallenge.server.Session;
import local.kamil.collibrachallenge.graph.Graph;

@Component
public class CommandProcessor {

	private static final String UNKNOWN_MESSAGE_RESPONSE = "SORRY, I DID NOT UNDERSTAND THAT";
	public static final String NODE_NOT_FOUND_MESSAGE = "ERROR: NODE NOT FOUND";

	private final Map<CommandType, BiConsumer<Command, Session>> strategies;

	@Autowired
	private final Graph graph;

	public CommandProcessor(Graph graph) {
		this.graph = graph;

		strategies = new HashMap<>();
		strategies.put(CommandType.HELLO, this::processHelloCommand);
		strategies.put(CommandType.ADD_NODE, this::processAddNodeCommand);
		strategies.put(CommandType.REMOVE_NODE, this::processRemoveNodeCommand);
		strategies.put(CommandType.ADD_EDGE, this::processAddEdgeCommand);
		strategies.put(CommandType.REMOVE_EDGE, this::processRemoveEdgeCommand);
		strategies.put(CommandType.SHORTEST_PATH, this::processShortestPathCommand);
		strategies.put(CommandType.CLOSER_THAN, this::processCloserThanCommand);
		strategies.put(CommandType.BYE, this::processByeMessage);
		strategies.put(CommandType.UNKNOWN, this::processUnknownMessage);
	}

	public void process(Command command, Session session) {
		final BiConsumer<Command, Session> strategy = strategies.get(command.getType());

		if (strategy != null) {
			strategy.accept(command, session);
		} else {
			throw new IllegalArgumentException("unsupported command type " + command.getType());
		}
	}

	private void processHelloCommand(Command command, Session session) {
		HelloCommand helloCommand = (HelloCommand) command;
		final String clientName = helloCommand.getClientName();

		session.setClientName(clientName);
		sendMessage(session.getOutput(), "HI " + session.getClientName());
	}

	private void processAddNodeCommand(Command command, Session session) {
		AddNodeCommand addNodeCommand = (AddNodeCommand) command;

		if (graph.addNode(addNodeCommand.getNodeName())) {
			sendMessage(session.getOutput(), "NODE ADDED");
		} else {
			sendMessage(session.getOutput(), "ERROR: NODE ALREADY EXISTS");
		}
	}

	private void processRemoveNodeCommand(Command command, Session session) {
		RemoveNodeCommand removeNodeCommand = (RemoveNodeCommand) command;

		if (graph.removeNode(removeNodeCommand.getNodeName())) {
			sendMessage(session.getOutput(), "NODE REMOVED");
		} else {
			sendMessage(session.getOutput(), NODE_NOT_FOUND_MESSAGE);
		}
	}

	private void processAddEdgeCommand(Command command, Session session) {
		AddEdgeCommand addEdgeCommand = (AddEdgeCommand) command;

		if (graph.addEdge(addEdgeCommand.getSourceNodeName(), addEdgeCommand.getTargetNodeName(), addEdgeCommand.getWeight())) {
			sendMessage(session.getOutput(), "EDGE ADDED");
		} else {
			sendMessage(session.getOutput(), NODE_NOT_FOUND_MESSAGE);
		}
	}

	private void processRemoveEdgeCommand(Command command, Session session) {
		RemoveEdgeCommand removeEdgeCommand = (RemoveEdgeCommand) command;

		if (graph.removeEdge(removeEdgeCommand.getSourceNodeName(), removeEdgeCommand.getTargetNodeName())) {
			sendMessage(session.getOutput(), "EDGE REMOVED");
		} else {
			sendMessage(session.getOutput(), NODE_NOT_FOUND_MESSAGE);
		}
	}

	private void processShortestPathCommand(Command command, Session session) {
		ShortestPathCommand shortestPathCommand = (ShortestPathCommand) command;

		Integer shortestDistance = graph.shortestDistance(shortestPathCommand.getSourceNodeName(), shortestPathCommand.getTargetNodeName());
		if (shortestDistance != null) {
			sendMessage(session.getOutput(), Integer.toString(shortestDistance));
		} else {
			sendMessage(session.getOutput(), NODE_NOT_FOUND_MESSAGE);
		}
	}

	private void processCloserThanCommand(Command command, Session session) {
		CloserThanCommand closerThanCommand = (CloserThanCommand) command;

		final List<String> nodesCloserThan = graph.nodesCloserThan(closerThanCommand.getNodeName(), closerThanCommand.getWeight());
		if (nodesCloserThan != null) {
			sendMessage(session.getOutput(), String.join(",", nodesCloserThan));
		} else {
			sendMessage(session.getOutput(), NODE_NOT_FOUND_MESSAGE);
		}
	}

	private void processByeMessage(Command command, Session session) {
		sendMessage(session.getOutput(), String.format("BYE %s, WE SPOKE FOR %d MS", session.getClientName(), session.getDuration()));
		session.end();
	}

	private void processUnknownMessage(Command command, Session session) {
		sendMessage(session.getOutput(), UNKNOWN_MESSAGE_RESPONSE);
	}

	public void sendMessage(PrintWriter out, String message) {
		out.println(message);
		out.flush();
	}
}

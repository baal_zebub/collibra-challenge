package local.kamil.collibrachallenge.graph;

import java.util.List;
import java.util.Optional;

import org.jgrapht.alg.shortestpath.DijkstraShortestPath;
import org.jgrapht.graph.DefaultWeightedEdge;
import org.jgrapht.graph.DirectedWeightedPseudograph;
import org.jgrapht.traverse.ClosestFirstIterator;
import org.springframework.stereotype.Component;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.Streams;

@Component
public class Graph {

	org.jgrapht.Graph<String, DefaultWeightedEdge> graph = new DirectedWeightedPseudograph<>(DefaultWeightedEdge.class);

	public boolean addNode(String nodeName) {
		synchronized (this) {
			return graph.addVertex(nodeName);
		}
	}

	public boolean addEdge(String sourceNodeName, String targetNodeName, int weight) {
		synchronized (this) {
			try {
				DefaultWeightedEdge edge = graph.addEdge(sourceNodeName, targetNodeName);
				graph.setEdgeWeight(edge, weight);
				return true;
			} catch (IllegalArgumentException e) {
				return false;
			}
		}
	}

	public boolean removeNode(String nodeName) {
		synchronized (this) {
			return graph.removeVertex(nodeName);
		}
	}

	public boolean removeEdge(String sourceNodeName, String targetNodeName) {
		synchronized (this) {
			if (containsNodes(sourceNodeName, targetNodeName)) {
				graph.removeAllEdges(sourceNodeName, targetNodeName);
				return true;
			}

			return false;
		}
	}

	public Integer shortestDistance(String sourceNodeName, String targetNodeName) {
		if (!containsNodes(sourceNodeName, targetNodeName)) {
			return null;
		}

		synchronized (this) {
			DijkstraShortestPath<String, DefaultWeightedEdge> shortestPath = new DijkstraShortestPath<>(graph);

			return Optional.ofNullable(shortestPath.getPaths(sourceNodeName))
				.map(paths -> paths.getWeight(targetNodeName))
				.map(Double::intValue)
				.orElse(Integer.MAX_VALUE);
		}
	}

	public List<String> nodesCloserThan(String sourceNode, int weight) {
		if (!graph.containsVertex(sourceNode)) {
			return null;
		}

		synchronized (this) {
			ClosestFirstIterator<String, DefaultWeightedEdge> closestFirstIterator = new ClosestFirstIterator<>(graph, sourceNode, weight - 1);
			return Streams.stream(closestFirstIterator)
				.filter(node -> !node.equals(sourceNode))
				.sorted()
				.collect(ImmutableList.toImmutableList());
		}
	}

	private synchronized boolean containsNodes(String sourceNodeName, String targetNodeName) {
		return graph.containsVertex(sourceNodeName) && graph.containsVertex(targetNodeName);
	}
}

package local.kamil.collibrachallenge.server;

import java.io.BufferedReader;
import java.io.PrintWriter;

import local.kamil.collibrachallenge.graph.Graph;

public class Session {

	private static final String INVALID_SESSION_MESSAGE = "session has been invalidated";

	private final String uuid;
	private final long startTimeMillis;
	private final BufferedReader input;
	private final PrintWriter output;
	private String clientName;
	private boolean ended;
	private boolean valid;
	private final Graph graph;

	public Session(String uuid, long startTimeMillis, BufferedReader input, PrintWriter output) {
		this.uuid = uuid;
		this.startTimeMillis = startTimeMillis;
		this.input = input;
		this.output = output;
		this.ended = false;
		this.valid = true;
		this.graph = new Graph();
	}

	public String getUuid() {
		return uuid;
	}

	public long getDuration() {
		return System.currentTimeMillis() - startTimeMillis;
	}

	public BufferedReader getInput() {
		if(valid) {
			return input;
		}

		throw new IllegalStateException(INVALID_SESSION_MESSAGE);
	}

	public PrintWriter getOutput() {
		if(valid) {
			return output;
		}

		throw new IllegalStateException(INVALID_SESSION_MESSAGE);
	}

	public String getClientName() {
		return clientName;
	}

	public void setClientName(String clientName) {
		this.clientName = clientName;
	}

	public boolean isEnded() {
		return ended;
	}

	public void end() {
		ended = true;
	}

	public void invalidate() {
		valid = false;
	}
}

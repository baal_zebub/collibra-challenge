package local.kamil.collibrachallenge.server;

import java.io.IOException;
import java.net.ServerSocket;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.stream.IntStream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import local.kamil.collibrachallenge.command.CommandProcessor;

@Service
public class Server {

	private static final Logger LOGGER = LoggerFactory.getLogger(Server.class);

	private final int port;
	private final int socketTimeout;
	private final int threadPoolSize;

	@Autowired
	private CommandProcessor commandProcessor;

	public Server(
			@Value("${server.port}") int port,
			@Value("${server.socket.timeout.ms}") int socketTimeout,
			@Value("${server.threads}") int threadPoolSize) {
		this.port = port;
		this.socketTimeout = socketTimeout;
		this.threadPoolSize = threadPoolSize;
	}

	public void start() throws IOException {
		ServerSocket serverSocket = new ServerSocket(port);

		LOGGER.info("listening on port " + port);

		ExecutorService executorService = Executors.newFixedThreadPool(threadPoolSize);
		IntStream.range(0, threadPoolSize)
			.forEach(i -> executorService.submit(new ServerThread(i, serverSocket, socketTimeout, commandProcessor)));
	}
}

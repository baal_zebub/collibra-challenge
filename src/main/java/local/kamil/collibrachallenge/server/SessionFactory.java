package local.kamil.collibrachallenge.server;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.UUID;

public class SessionFactory {

	public static Session createSession(Socket clientSocket) throws IOException {
		final BufferedReader input = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
		PrintWriter output = new PrintWriter(clientSocket.getOutputStream());

		return new Session(UUID.randomUUID().toString(), System.currentTimeMillis(), input, output);
	}
}

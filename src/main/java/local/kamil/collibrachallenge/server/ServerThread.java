package local.kamil.collibrachallenge.server;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketTimeoutException;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import local.kamil.collibrachallenge.command.ByeCommand;
import local.kamil.collibrachallenge.command.Command;
import local.kamil.collibrachallenge.command.CommandFactory;
import local.kamil.collibrachallenge.command.CommandProcessor;

public class ServerThread extends Thread {

	private static final Logger LOGGER = LoggerFactory.getLogger(ServerThread.class);

	private final int threadNumber;
	private final ServerSocket serverSocket;
	private final CommandProcessor commandProcessor;
	private final int socketTimeout;

	public ServerThread(int threadNumber, ServerSocket serverSocket, int socketTimeout, CommandProcessor commandProcessor) {
		this.threadNumber = threadNumber;
		this.serverSocket = serverSocket;
		this.socketTimeout = socketTimeout;
		this.commandProcessor = commandProcessor;
	}

	@Override
	public void run() {
		try {
			while (true) {
				handleSession();
			}
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

	private void handleSession() throws IOException {
		Socket clientSocket = createClientSocket();
		final Session session = SessionFactory.createSession(clientSocket);

		beginCommunication(session);
		exchangeMessages(session);
		endCommunication(session, clientSocket);
	}

	private Socket createClientSocket() throws IOException {
		Socket clientSocket;

		synchronized (serverSocket) {
			clientSocket = serverSocket.accept();
		}

		LOGGER.debug("[{}] starting session", threadNumber);
		clientSocket.setSoTimeout(socketTimeout);
		return clientSocket;
	}

	private void beginCommunication(Session session) {
		final String helloMessage = "HI, I AM " + session.getUuid();
		commandProcessor.sendMessage(session.getOutput(), helloMessage);
	}

	private void exchangeMessages(Session session) throws IOException {
		String clientMessage;

		try {
			while (!session.isEnded() && (clientMessage = session.getInput().readLine()) != null) {
				final Command command = CommandFactory.create(clientMessage);

				commandProcessor.process(command, session);
			}
		} catch (SocketTimeoutException e) {
			if (StringUtils.isNotBlank(session.getClientName())) {
				commandProcessor.process(new ByeCommand(), session);
			}
		}
	}

	private void endCommunication(Session session, Socket clientSocket) throws IOException {
		LOGGER.debug("[{}] ending session after {} ms", threadNumber, session.getDuration());
		session.invalidate();
		clientSocket.close();
	}
}

package local.kamil.collibrachallenge;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import local.kamil.collibrachallenge.server.Server;

@SpringBootApplication
public class CollibraChallengeApplication implements CommandLineRunner {

	@Autowired
	private Server server;

	public static void main(String[] args) {
		SpringApplication.run(CollibraChallengeApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		server.start();
	}
}

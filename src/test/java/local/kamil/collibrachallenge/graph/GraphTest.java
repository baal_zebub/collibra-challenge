package local.kamil.collibrachallenge.graph;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;

import java.util.List;

import org.junit.jupiter.api.Test;

import com.google.common.collect.ImmutableList;

class GraphTest {

	private static final String SOURCE_NODE = "x";
	private static final String TARGET_NODE = "y";
	private static final String FIRST_INTERMEDIATE_NODE = "z";
	private static final String SECOND_INTERMEDIATE_NODE = "w";

	private final Graph graph = new Graph();

	@Test
	void shouldAddNodeToEmptyGraph() {
		boolean result = graph.addNode(SOURCE_NODE);

		assertThat(result).isTrue();
	}

	@Test
	void shouldAddNodeToNonEmptyGraph() {
		graph.addNode(TARGET_NODE);

		boolean result = graph.addNode(SOURCE_NODE);

		assertThat(result).isTrue();
	}

	@Test
	void shouldNotAddNodeWhichExistsInGraph() {
		graph.addNode(SOURCE_NODE);

		boolean result = graph.addNode(SOURCE_NODE);

		assertThat(result).isFalse();
	}

	@Test
	void shouldThrowNpeWhenAddingNullNode() {
		assertThatExceptionOfType(NullPointerException.class)
			.isThrownBy(() -> graph.addNode(null));
	}

	@Test
	void shouldAddNewEdgeForNodesWithoutEdge() {
		graph.addNode(SOURCE_NODE);
		graph.addNode(TARGET_NODE);

		boolean result = graph.addEdge(SOURCE_NODE, TARGET_NODE, 1);

		assertThat(result).isTrue();
	}

	@Test
	void shouldAddEdgeWithNodesAlreadyHavingOne() {
		graph.addNode(SOURCE_NODE);
		graph.addNode(TARGET_NODE);
		graph.addEdge(SOURCE_NODE, TARGET_NODE, 1);

		boolean result = graph.addEdge(SOURCE_NODE, TARGET_NODE, 2);

		assertThat(result).isTrue();
	}

	@Test
	void shouldNotAddEdgeToGraphMissingSourceNode() {
		graph.addNode(TARGET_NODE);

		boolean result = graph.addEdge(SOURCE_NODE, TARGET_NODE, 1);

		assertThat(result).isFalse();
	}

	@Test
	void shouldNotAddEdgeToGraphMissingTargetNode() {
		graph.addNode(SOURCE_NODE);

		boolean result = graph.addEdge(SOURCE_NODE, TARGET_NODE, 1);

		assertThat(result).isFalse();
	}

	@Test
	public void shouldThrowNpeWhenAddingEdgeWithNullSourceNode() {
		graph.addNode(SOURCE_NODE);
		graph.addNode(TARGET_NODE);

		assertThatExceptionOfType(NullPointerException.class)
			.isThrownBy(() -> graph.addEdge(null, TARGET_NODE, 1));
	}

	@Test
	public void shouldThrowNpeWhenAddingEdgeWithNullTargetNode() {
		graph.addNode(SOURCE_NODE);
		graph.addNode(TARGET_NODE);

		assertThatExceptionOfType(NullPointerException.class)
			.isThrownBy(() -> graph.addEdge(SOURCE_NODE, null, 1));
	}

	@Test
	void shouldRemoveExistingNode() {
		graph.addNode(SOURCE_NODE);

		boolean result = graph.removeNode(SOURCE_NODE);

		assertThat(result).isTrue();
	}

	@Test
	void shouldNotRemoveNodeFromEmptyGraph() {
		boolean result = graph.removeNode(SOURCE_NODE);

		assertThat(result).isFalse();
	}

	@Test
	void shouldNotRemoveNonExistingNodeFromNonEmptyGraph() {
		graph.addNode(SOURCE_NODE);

		boolean result = graph.removeNode(TARGET_NODE);

		assertThat(result).isFalse();
	}

	@Test
	void shouldRemoveSucceedForSingleEdge() {
		graph.addNode(SOURCE_NODE);
		graph.addNode(TARGET_NODE);
		graph.addEdge(SOURCE_NODE, TARGET_NODE, 1);

		boolean result = graph.removeEdge(SOURCE_NODE, TARGET_NODE);

		assertThat(result).isTrue();
	}

	@Test
	void shouldRemoveSucceedForMultipleEdges() {
		graph.addNode(SOURCE_NODE);
		graph.addNode(TARGET_NODE);
		graph.addEdge(SOURCE_NODE, TARGET_NODE, 1);
		graph.addEdge(SOURCE_NODE, TARGET_NODE, 2);
		graph.addEdge(SOURCE_NODE, TARGET_NODE, 3);

		boolean firstResult = graph.removeEdge(SOURCE_NODE, TARGET_NODE);
		boolean secondResult = graph.removeEdge(SOURCE_NODE, TARGET_NODE);
		boolean thirdResult = graph.removeEdge(SOURCE_NODE, TARGET_NODE);

		assertThat(firstResult).isTrue();
		assertThat(secondResult).isTrue();
		assertThat(thirdResult).isTrue();
	}

	@Test
	void shouldRemoveSucceedForNoEdges() {
		graph.addNode(SOURCE_NODE);
		graph.addNode(TARGET_NODE);

		boolean result = graph.removeEdge(SOURCE_NODE, TARGET_NODE);

		assertThat(result).isTrue();
	}

	@Test
	void shouldRemoveSucceedForMoreCallsThanEdges() {
		graph.addNode(SOURCE_NODE);
		graph.addNode(TARGET_NODE);
		graph.addEdge(SOURCE_NODE, TARGET_NODE, 1);

		boolean firstResult = graph.removeEdge(SOURCE_NODE, TARGET_NODE);
		boolean secondResult = graph.removeEdge(SOURCE_NODE, TARGET_NODE);
		boolean thirdResult = graph.removeEdge(SOURCE_NODE, TARGET_NODE);

		assertThat(firstResult).isTrue();
		assertThat(secondResult).isTrue();
		assertThat(thirdResult).isTrue();
	}

	@Test
	void shouldRemoveNotSucceedForEmptyGraph() {
		boolean result = graph.removeEdge(SOURCE_NODE, TARGET_NODE);

		assertThat(result).isFalse();
	}

	@Test
	void shouldRemoveNotSucceedForSourceNodeMissing() {
		graph.addNode(TARGET_NODE);

		boolean result = graph.removeEdge(SOURCE_NODE, TARGET_NODE);

		assertThat(result).isFalse();
	}

	@Test
	void shouldRemoveNotSucceedForTargetNodeMissing() {
		graph.addNode(SOURCE_NODE);

		boolean result = graph.removeEdge(SOURCE_NODE, TARGET_NODE);

		assertThat(result).isFalse();
	}

	@Test
	void shouldReturnShortestDistanceForDirectSingleEdge() {
		final int expectedShortestDistance = 8;

		graph.addNode(SOURCE_NODE);
		graph.addNode(TARGET_NODE);
		graph.addEdge(SOURCE_NODE, TARGET_NODE, expectedShortestDistance);

		Integer result = graph.shortestDistance(SOURCE_NODE, TARGET_NODE);

		assertThat(result).isEqualTo(expectedShortestDistance);
	}

	@Test
	void shouldReturnShortestDistanceForMultipleDirectEdges() {
		final int expectedShortestDistance = 8;

		graph.addNode(SOURCE_NODE);
		graph.addNode(TARGET_NODE);
		graph.addEdge(SOURCE_NODE, TARGET_NODE, 42);
		graph.addEdge(SOURCE_NODE, TARGET_NODE, expectedShortestDistance);
		graph.addEdge(SOURCE_NODE, TARGET_NODE, 13);

		Integer result = graph.shortestDistance(SOURCE_NODE, TARGET_NODE);

		assertThat(result).isEqualTo(expectedShortestDistance);
	}

	@Test
	void shouldReturnShortestDistanceForSingleIndirectPath() {
		final int expectedShortestDistance = 55;

		graph.addNode(SOURCE_NODE);
		graph.addNode(FIRST_INTERMEDIATE_NODE);
		graph.addNode(TARGET_NODE);
		graph.addEdge(SOURCE_NODE, FIRST_INTERMEDIATE_NODE, 42);
		graph.addEdge(FIRST_INTERMEDIATE_NODE, TARGET_NODE, 13);

		Integer result = graph.shortestDistance(SOURCE_NODE, TARGET_NODE);

		assertThat(result).isEqualTo(expectedShortestDistance);
	}

	@Test
	void shouldReturnShortestDistanceForMultipleIndirectPaths() {
		final int expectedShortestDistance = 51;

		graph.addNode(SOURCE_NODE);
		graph.addNode(FIRST_INTERMEDIATE_NODE);
		graph.addNode(TARGET_NODE);
		graph.addEdge(SOURCE_NODE, FIRST_INTERMEDIATE_NODE, 42);
		graph.addEdge(FIRST_INTERMEDIATE_NODE, TARGET_NODE, 13);
		graph.addEdge(FIRST_INTERMEDIATE_NODE, TARGET_NODE, 9);

		Integer result = graph.shortestDistance(SOURCE_NODE, TARGET_NODE);

		assertThat(result).isEqualTo(expectedShortestDistance);
	}

	@Test
	void shouldReturnIntegerMaxValueForNoPathBetweenEdges() {

		graph.addNode(SOURCE_NODE);
		graph.addNode(FIRST_INTERMEDIATE_NODE);
		graph.addNode(TARGET_NODE);
		graph.addEdge(FIRST_INTERMEDIATE_NODE, TARGET_NODE, 13);
		graph.addEdge(FIRST_INTERMEDIATE_NODE, TARGET_NODE, 9);

		Integer result = graph.shortestDistance(SOURCE_NODE, TARGET_NODE);

		assertThat(result).isEqualTo(Integer.MAX_VALUE);
	}

	@Test
	void shouldReturnIntegerMaxValueForCycleBetweenEdges() {

		graph.addNode(SOURCE_NODE);
		graph.addNode(FIRST_INTERMEDIATE_NODE);
		graph.addNode(SECOND_INTERMEDIATE_NODE);
		graph.addNode(TARGET_NODE);
		graph.addEdge(SOURCE_NODE, FIRST_INTERMEDIATE_NODE, 42);
		graph.addEdge(FIRST_INTERMEDIATE_NODE, SECOND_INTERMEDIATE_NODE, 13);
		graph.addEdge(SECOND_INTERMEDIATE_NODE, FIRST_INTERMEDIATE_NODE, 9);

		Integer result = graph.shortestDistance(SOURCE_NODE, TARGET_NODE);

		assertThat(result).isEqualTo(Integer.MAX_VALUE);
	}

	@Test
	void shouldNullForSourceNodeNotInGraph() {

		graph.addNode(FIRST_INTERMEDIATE_NODE);
		graph.addNode(TARGET_NODE);
		graph.addEdge(FIRST_INTERMEDIATE_NODE, TARGET_NODE, 13);

		Integer result = graph.shortestDistance(SOURCE_NODE, TARGET_NODE);

		assertThat(result).isNull();
	}

	@Test
	void shouldNullForTargetNodeNotInGraph() {

		graph.addNode(SOURCE_NODE);
		graph.addNode(FIRST_INTERMEDIATE_NODE);
		graph.addEdge(FIRST_INTERMEDIATE_NODE, TARGET_NODE, 13);
		graph.addEdge(FIRST_INTERMEDIATE_NODE, TARGET_NODE, 9);

		Integer result = graph.shortestDistance(SOURCE_NODE, TARGET_NODE);

		assertThat(result).isNull();
	}

	@Test
	void shouldReturnNodesCloserThanForSingleEdgesPerTargetNode() {

		graph.addNode(SOURCE_NODE);
		graph.addNode(FIRST_INTERMEDIATE_NODE);
		graph.addNode(SECOND_INTERMEDIATE_NODE);
		graph.addNode(TARGET_NODE);

		graph.addEdge(SOURCE_NODE, FIRST_INTERMEDIATE_NODE, 13);
		graph.addEdge(SOURCE_NODE, SECOND_INTERMEDIATE_NODE, 20);
		graph.addEdge(SOURCE_NODE, TARGET_NODE, 4);

		final List<String> result = graph.nodesCloserThan(SOURCE_NODE, 15);

		assertThat(result).isEqualTo(ImmutableList.of("y", "z"));
	}

	@Test
	void shouldNodesCloserThanIncludeNodeWithMultipleEdges() {

		graph.addNode(SOURCE_NODE);
		graph.addNode(FIRST_INTERMEDIATE_NODE);
		graph.addNode(SECOND_INTERMEDIATE_NODE);
		graph.addNode(TARGET_NODE);

		graph.addEdge(SOURCE_NODE, FIRST_INTERMEDIATE_NODE, 13);
		graph.addEdge(SOURCE_NODE, SECOND_INTERMEDIATE_NODE, 20);
		graph.addEdge(SOURCE_NODE, SECOND_INTERMEDIATE_NODE, 11);
		graph.addEdge(SOURCE_NODE, TARGET_NODE, 4);

		final List<String> result = graph.nodesCloserThan(SOURCE_NODE, 15);

		assertThat(result).isEqualTo(ImmutableList.of("w", "y", "z"));
	}

	@Test
	public void shouldNodesCloserThanReturnNullForSourceNodeNotInGraph() {
		graph.addNode(TARGET_NODE);

		final List<String> result = graph.nodesCloserThan(SOURCE_NODE, 15);

		assertThat(result).isNull();
	}
}
package local.kamil.collibrachallenge.command;

import java.util.stream.Stream;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

public class HelloCommandFactoryTest {

	@ParameterizedTest
	@MethodSource("parametersForMessageMatchTest")
	public void shouldMatchCorrectHelloMessage(String rawMessage) {
		final boolean result = CommandFactory.HELLO.canCreateFrom(rawMessage);

		Assertions.assertThat(result).isTrue();
	}

	@ParameterizedTest
	@MethodSource("parametersForMessageMismatchTest")
	public void shouldNotMatchAnIncorrectHelloMessage(String rawMessage) {
		final boolean result = CommandFactory.HELLO.canCreateFrom(rawMessage);

		Assertions.assertThat(result).isFalse();
	}

	@ParameterizedTest
	@MethodSource("parametersForCorrectMessageCreationTest")
	public void shouldCreateCorrectHelloMessage(String rawMessage, String expectedClientName) {
		final Command message = CommandFactory.HELLO.createMessageFrom(rawMessage);

		Assertions.assertThat(message).isNotNull();
		Assertions.assertThat(message.getType()).isEqualTo(CommandType.HELLO);

		final HelloCommand helloMessage = (HelloCommand) message;

		Assertions.assertThat(helloMessage.getClientName()).isEqualTo(expectedClientName);
	}

	@ParameterizedTest
	@MethodSource("parametersForMessageMismatchTest")
	public void shouldNotCreateHelloMessage(String rawMessage) {
		Assertions.assertThatExceptionOfType(IllegalArgumentException.class)
			.isThrownBy(() -> CommandFactory.HELLO.createMessageFrom(rawMessage));
	}

	private static Stream<Arguments> parametersForMessageMatchTest() {
		return Stream.of(
			Arguments.of("HI, I AM AL4"),
			Arguments.of("HI, I AM AL4-2"),
			Arguments.of("HI, I AM -")
		);
	}

	private static Stream<Arguments> parametersForMessageMismatchTest() {
		return Stream.of(
			Arguments.of(""),
			Arguments.of("HI, I AM"),
			Arguments.of("HI, I AM "),
			Arguments.of("HI, I AM  "),
			Arguments.of("HI, I AM  &amp;"),
			Arguments.of("Lorem ipsum")
		);
	}

	private static Stream<Arguments> parametersForCorrectMessageCreationTest() {
		return Stream.of(
			Arguments.of("HI, I AM AL4", "AL4"),
			Arguments.of("HI, I AM AL4-2", "AL4-2"),
			Arguments.of("HI, I AM -", "-")
		);
	}

}
# Starting the app
1. Build the jar
   ```shell
   ./mvnw clean package
   ```
1. Run it
   ```shell
   java -jar target/collibra-challenge-1.0.0-SNAPSHOT.jar
   ```
   
# Adjusting the log level
To disable logging of start, end and duration of each TCP session, change value of `logging.level.local.kamil.collibrachallenge` property in `application.properties` to `INFO`.